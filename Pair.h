// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-21
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.
#ifndef PAIR_H
#define PAIR_H

template <typename A, typename B>
class Pair {
public:
	Pair() = default;

	Pair(const Pair& src)
		: m_key(src.m_key), m_val(src.m_val)
	{
		
	}

	Pair& operator=(const Pair& rhs) {
        using std::swap;

		Pair tmp{ rhs };

		swap(m_key, tmp.m_key);
		swap(m_val, tmp.m_val);

		return *this;
	}

	Pair(const A& key, const B& val)
		: m_key(key), m_val(val)
	{
		
	}

	const A& getKey() const { return m_key; }
	const B& getValue() const { return m_val; }

private:
	A m_key{};
	B m_val{};
};

#endif // end PAIR_H
