#include "Pair.h"

#include <string>
#include <cassert>
#include <iostream>

int main() {
	Pair<int, std::string> good_pair{5, "foo"};
	assert(good_pair.getKey() == 5);
	assert(good_pair.getValue() == "foo");
	std::cout << "Pair initializes with rvalue arguments\n";
	std::cout << "getKey() works\n";
	std::cout << "getValue() works\n";

	int key = 10;
	std::string val = "bar";
	Pair<int, std::string> good_pair2{key, val};
	std::cout << "Pair initializes with lvalue arguments\n";
	assert(key == 10);
	assert(val == "bar");
	std::cout << "and lvalues aren't mutated\n";

	Pair<int, std::string*> default_pair{};
	assert(default_pair.getKey() == 0);
	assert(default_pair.getValue() == nullptr);
	std::cout << "Pair default initializes by calling a default constructor for each data member";

	return 0;
}