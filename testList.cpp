#include "List.h"

#include <iostream>
#include <stdexcept>
#include <cassert>

int main() {
	// Test default constructor and size()
	List<int, 2> default_list;
	assert(default_list.size() == 0);
	std::cout << "Default initialized List has size() = 0\n";

	// Test good indexing
	constexpr size_t size = 3;
	List<int, size> good_list;
	good_list += 1;
	good_list += 99;
	int int_lvalue = 3;
	good_list += int_lvalue;
	assert(int_lvalue == 3);
	std::cout << "List's operator+=(src) does not mutate src\n";
	assert(good_list[0] == 1);
	assert(good_list[1] == 99);
	assert(good_list[2] == 3);
	std::cout << "List's index operator works with in-range index\n";

	// Test bad indexing
	List<int, size> bad_list;
	// bad_list += "hello"; // Passing incompatible type does raise compile time error
	try {
		bad_list[4];
	}
	catch (std::out_of_range) {
		std::cout << "Accessing List with invalid access raises out of range error\n";
	}
	try {
		bad_list[-1];
	}
	catch (std::out_of_range) {
		std::cout << "Accessing List with invalid access raises out of range error\n";
	}

	// Test bad adding
	for (size_t i = 0; i < bad_list.size() + 1; ++i) {
		try {
			bad_list += 1;
		}
		catch (std::length_error) {
			std::cout << "Adding more than the List's max capacity raises length error\n";
		}
	}

	return 0;
}