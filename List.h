// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-21
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.
#ifndef LIST_H
#define LIST_H

#include <stdexcept>

template<typename T, size_t N>
class List {
public:
	List() = default;

	List(const List&) = delete;
	List& operator=(const List&) = delete;

	size_t size() const { return m_count; }

	const T& operator[](int i) const {
		if (i < 0 || i >= N) {
			throw std::out_of_range("Found an index out side of the List's range");
		}

		return m_list[i];
	}

	void operator+=(const T& rhs) {
		if (m_count >= N) {
			throw std::length_error("Can't add more to the List");
		}
		
		m_list[m_count] = rhs;
		++m_count;
	}

private:
	T m_list[N];
	size_t m_count = 0;	
};

#endif // end LIST_H
